## Activity middleware
This middleware forwards requests received to the app on its endpoints to Google PubSub.
## Configuration
The middleware must be installed as last in the middleware list to function properly. In your django config you MUST have the following settings:  
1. ACTIVITY_PUBLISH_TOPIC - the PubSub topic to which the middleware will publish
2. ACTIVITY_STATUS - wheter the middleware should be active or not.
Optional settings:  
3. ACTIVITY_EXEMPT_PATHS - a list of paths from which to NOT gather requests. If nothing is specified it will collect from all paths
4. ACTIVITY_USER_ATTRIBUTE - the attribute that should be used for passing the user ID in the event
