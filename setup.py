from setuptools import setup


with open('README.md', 'r') as f:
    readme = f.read()


setup(name='django_activity',
      version='0.2.0',
      description='Activity django middleware - library for emitting PubSub events for user actions',
      long_description=readme,
      long_description_content_type='text/markdown',
      url='http://gitlab.com/atkozhuharov/django_activity',
      author='Atanas K',
      author_email='atkozhuharov@gmail.com',
      license='MIT',
      packages=['django_activity'])

