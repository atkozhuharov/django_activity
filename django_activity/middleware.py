import json
import re
from google.cloud import pubsub_v1
from google.api_core.exceptions import NotFound
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured


class ActivityMiddleware:
    """Middleware to emit events to PubSub."""
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        for i in ["ACTIVITY_STATUS", "ACTIVITY_PUBLISH_TOPIC"]:
            if not hasattr(settings, i):
                raise ImproperlyConfigured("Missing setting {}".format(i))
        # Check if it's a GET request - we don't want to emit events for them
        activity_status = settings.ACTIVITY_STATUS
        if request.method != "GET" and activity_status:
            full_path = request.get_full_path()
            emit = True
            # Check for exempt paths
            if hasattr(settings, "ACTIVITY_EXEMPT_PATHS"):
                for i in settings.ACTIVITY_EXEMPT_PATHS:
                    if re.search(re.compile(i), full_path):
                        emit = False
                        break
            if emit:
                body = request.body.decode()
                data = {"method": request.method, "path": full_path,
                        "data": body}
                if hasattr(settings, "ACTIVITY_USER_ATTRIBUTE"):
                    # If we have a user attribute from settings - get that from
                    # the request
                    if hasattr(request, settings.ACTIVITY_USER_ATTRIBUTE):
                        data["user"] = getattr(request, settings.ACTIVITY_USER_ATTRIBUTE)
        else:
            emit = False
        # Return the response
        response = self.get_response(request)
        if emit:
            if request.method == "POST":
                data["data"] = response.data
            try:
                publisher = pubsub_v1.PublisherClient()
                publisher.publish(topic=settings.ACTIVITY_PUBLISH_TOPIC,
                                  data=json.dumps(data).encode())
            except NotFound:
                # Topic doesn't exist - create it
                publisher.create_topic(settings.ACTIVITY_PUBLISH_TOPIC,
                                       timeout=3)
                publisher.publish(topic=settings.ACTIVITY_PUBLISH_TOPIC,
                                  data=json.dumps(data).encode())
        return response
